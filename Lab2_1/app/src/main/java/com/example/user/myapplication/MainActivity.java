package com.example.user.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView resView;
    private EditText X;
    private EditText Y;
    private EditText Z;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resView = (TextView) findViewById(R.id.textView2);
        X = (EditText) findViewById(R.id.editText5);
        Y = (EditText) findViewById(R.id.editText6);
        Z = (EditText) findViewById(R.id.editText8);
        X.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    calculate();
                }
            }
        });
        Y.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    calculate();
                }
            }
        });
        Z.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    calculate();
                }
            }
        });
    }

    public void buttonClick(View v) {
        calculate();
    }

    private void calculate(){
        double x=0, y=0, z=0;
        if (X.getText().toString().compareTo("") != 0) {
            try {
                x = Double.parseDouble(X.getText().toString());
            } catch (Exception ex) {
                X.requestFocus();
            }
        }
        if (Y.getText().toString().compareTo("") != 0) {
            try {
                y = Double.parseDouble(Y.getText().toString());
            } catch (Exception ex) {
                Y.requestFocus();
            }
        }
        if (Z.getText().toString().compareTo("") != 0) {
            try {
                z = Double.parseDouble(Z.getText().toString());
            } catch (Exception ex) {
                Z.requestFocus();
            }
        }
        double V = x * y * z;
        double S = Math.sqrt(x * x + y * y + z * z);
        String resStr = String.format("Объем = %.3f Площадь = %.3f", V, S);
        resView.setText(resStr);
    }
}
